
Python version to use: python3.6


Create Virtual environment:
```
virtualenv -p /usr/local/bin/python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
```

run program: 
```
python main.py
```
